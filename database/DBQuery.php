<?php

// users
function find_produit_by_id($id, $options=[]) {
global $db;

$sql = "SELECT * FROM produit ";
$sql .= "WHERE id_prod='" . db_escape($db, $id) . "' ";

$result = mysqli_query($db, $sql);
confirm_result_set($result);
$subject = mysqli_fetch_assoc($result);
mysqli_free_result($result);
return $subject; // returns an assoc. array
}
function find_all_products() {
    global $db;

    $sql = "SELECT * FROM produit ";
    $sql .= "ORDER BY nom ASC";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    return $result;
}


function find_product_type($product_type) {
    global $db;

    $sql = "SELECT * FROM produit ";
    $sql .= "WHERE type_produit='" . db_escape($db, $product_type) . "' ";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $user = mysqli_fetch_assoc($result); // find first
    mysqli_free_result($result);
    return $user; // returns an assoc. array
}

function find_product_name($product_name) {
    global $db;

    $sql = "SELECT * FROM produit ";
    $sql .= "WHERE nom_produit='" . db_escape($db, $product_name) . "' ";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $user = mysqli_fetch_assoc($result); // find first
    mysqli_free_result($result);
    return $user; // returns an assoc. array
}

function find_product_size($product_size) {
    global $db;

    $sql = "SELECT * FROM produit ";
    $sql .= "WHERE taille='" . db_escape($db, $product_size) . "' ";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $user = mysqli_fetch_assoc($result); // find first
    mysqli_free_result($result);
    return $user; // returns an assoc. array
}
function find_product_brand($product_brand) {
    global $db;

    $sql = "SELECT * FROM produit ";
    $sql .= "WHERE marque='" . db_escape($db, $product_brand) . "' ";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $user = mysqli_fetch_assoc($result); // find first
    mysqli_free_result($result);
    return $user; // returns an assoc. array
}
function find_product_color($product_color) {
    global $db;

    $sql = "SELECT * FROM produit ";
    $sql .= "WHERE couleur='" . db_escape($db, $product_color) . "' ";
    $result = mysqli_query($db, $sql);
    confirm_result_set($result);
    $user = mysqli_fetch_assoc($result); // find first
    mysqli_free_result($result);
    return $user; // returns an assoc. array
}

function validate_product($user) {

    if(is_blank($user['Nom_produit'])) {
        $errors[] = "product name cannot be blank.";
    } elseif (!has_length($user['Nom_produit'], array('min' => 2, 'max' => 255))) {
        $errors[] = "product name must be between 2 and 255 characters.";
    }

    if(is_blank($user['Type_produit'])) {
        $errors[] = "product type cannot be blank.";
    } elseif (!has_length($user['Type_produit'], array('min' => 2, 'max' => 255))) {
        $errors[] = "product type must be between 2 and 255 characters.";
    }

    if(is_blank($user['Marque'])) {
        $errors[] = "Marque cannot be blank.";
    } elseif (!has_length($user['email'], array('max' => 255))) {
        $errors[] = "Marque must be less than 255 characters.";
    }

    if(is_blank($user['Taille'])) {
        $errors[] = "taille cannot be blank.";
    } elseif (!has_length($user['Taille'], array('max' => 255))) {
        $errors[] = "taille must be less than 255 characters.";
    }
    if(is_blank($user['Couleur'])) {
        $errors[] = "couleur cannot be blank.";
    } elseif (!has_length($user['Couleur'], array('max' => 255))) {
        $errors[] = "couleur must be less than 255 characters.";
    }
    if(is_blank($user['Description'])) {
        $errors[] = "description cannot be blank.";
    } elseif (!has_length($user['Description'], array('max' => 255))) {
        $errors[] = "description must be less than 255 characters.";
    }
    if(is_blank($user['Quantité'])) {
        $errors[] = "Quantité cannot be blank.";
    } elseif (!has_length($user['Quantité'], array('max' => 255))) {
        $errors[] = "Quantité must be less than 255 characters.";
    }
    if(is_blank($user['Prix'])) {
        $errors[] = "prix cannot be blank.";
    } elseif (!has_length($user['Prix'], array('max' => 255))) {
        $errors[] = "prix must be less than 255 characters.";
    }


    return $errors;
}

function insert_produit($user) {
    global $db;

    $errors = validate_produit($user);
    if (!empty($errors)) {
        return $errors;
    }

    $sql = "INSERT INTO produit ";
    $sql .= "(Nom_produit,Type_produit,Marque,Taille,Couleur,Description,Quantite,Prix) ";
    $sql .= "VALUES (";
    $sql .= "'" . db_escape($db, $user['Nom_produit']) . "',";
    $sql .= "'" . db_escape($db, $user['Type_produit']) . "',";
    $sql .= "'" . db_escape($db, $user['Marque']) . "',";
    $sql .= "'" . db_escape($db, $user['Taille']) . "',";
    $sql .= "'" . db_escape($db, $user['Couleur']) . "',";
    $sql .= "'" . db_escape($db, $user['Description']) . "',";
    $sql .= "'" . db_escape($db, $user['Quantite']) . "',";
    $sql .= "'" . db_escape($db, $user['Prix']) . "',";
    $sql .= ")";
    $result = mysqli_query($db, $sql);

// For INSERT statements, $result is true/false
    if($result) {
        return true;
    } else {
// INSERT failed
        echo mysqli_error($db);
        db_disconnect($db);
        exit;
    }
}

function update_produit($user) {
    global $db;

    $errors = validate_user($user);
    if (!empty($errors)) {
        return $errors;
    }

    $sql = "UPDATE produit SET ";
    $sql .= "Nom_produit='" . db_escape($db, $user['Nom_produit']) . "', ";
    $sql .= "Type_produit='" . db_escape($db, $user['Type_produit']) . "', ";
    $sql .= "Marque='" . db_escape($db, $user['Marque']) . "', ";
    $sql .= "Taille='" . db_escape($db, $user['Taille']) . "',";
    $sql .= "Couleur='" . db_escape($db, $user['Couleur']) . "',";
    $sql .= "Description='" . db_escape($db, $user['Description']) . "',";
    $sql .= "Quantite='" . db_escape($db, $user['Quantite']) . "',";
    $sql .= "Prix='" . db_escape($db, $user['Prix']) . "',";
    $sql .= "WHERE id_prod='" . db_escape($db, $user['id']) . "' ";
    $sql .= "LIMIT 1";
    $result = mysqli_query($db, $sql);

// For UPDATE statements, $result is true/false
    if($result) {
        return true;
    } else {
// UPDATE failed
        echo mysqli_error($db);
        db_disconnect($db);
        exit;
    }
}

function delete_produit($produit) {
    global $db;

    $sql = "DELETE FROM produit ";
    $sql .= "WHERE id_prod='" . db_escape($db, $produit['id']) . "' ";
    $sql .= "LIMIT 1;";
    $result = mysqli_query($db, $sql);

// For DELETE statements, $result is true/false
    if($result) {
        return true;
    } else {
// DELETE failed
        echo mysqli_error($db);
        db_disconnect($db);
        exit;
    }
}

// users

// Find all users, ordered last_name, first_name
function find_all_users() {
global $db;

$sql = "SELECT * FROM personne ";
$sql .= "ORDER BY nom ASC, prenom ASC";
$result = mysqli_query($db, $sql);
confirm_result_set($result);
return $result;
}

function find_user_by_id($id) {
global $db;

$sql = "SELECT * FROM personne ";
$sql .= "WHERE id_pers='" . db_escape($db, $id) . "' ";
$sql .= "LIMIT 1";
$result = mysqli_query($db, $sql);
confirm_result_set($result);
$user = mysqli_fetch_assoc($result); // find first
mysqli_free_result($result);
return $user; // returns an assoc. array
}

function find_personne_by_email($username) {
global $db;

$sql = "SELECT * FROM personne ";
$sql .= "WHERE adr_mail='" . db_escape($db, $username) . "' ";
$sql .= "LIMIT 1";
$result = mysqli_query($db, $sql);
confirm_result_set($result);
$user = mysqli_fetch_assoc($result); // find first
mysqli_free_result($result);
return $user; // returns an assoc. array
}

function validate_user($user) {

if(is_blank($user['nom'])) {
$errors[] = "First name cannot be blank.";
} elseif (!has_length($user['first_name'], array('min' => 2, 'max' => 255))) {
$errors[] = "First name must be between 2 and 255 characters.";
}

if(is_blank($user['prenom'])) {
$errors[] = "Last name cannot be blank.";
} elseif (!has_length($user['last_name'], array('min' => 2, 'max' => 255))) {
$errors[] = "Last name must be between 2 and 255 characters.";
}

if(is_blank($user['email'])) {
$errors[] = "Email cannot be blank.";
} elseif (!has_length($user['email'], array('max' => 255))) {
$errors[] = "Last name must be less than 255 characters.";
} elseif (!has_valid_email_format($user['email'])) {
$errors[] = "Email must be a valid format.";
}


if(is_blank($user['password'])) {
$errors[] = "Password cannot be blank.";
} elseif (!has_length($user['password'], array('min' => 12))) {
$errors[] = "Password must contain 12 or more characters";
} elseif (!preg_match('/[A-Z]/', $user['password'])) {
$errors[] = "Password must contain at least 1 uppercase letter";
} elseif (!preg_match('/[a-z]/', $user['password'])) {
$errors[] = "Password must contain at least 1 lowercase letter";
} elseif (!preg_match('/[0-9]/', $user['password'])) {
$errors[] = "Password must contain at least 1 number";
} elseif (!preg_match('/[^A-Za-z0-9\s]/', $user['password'])) {
$errors[] = "Password must contain at least 1 symbol";
}

return $errors;
}

function insert_user($user) {
global $db;

$errors = validate_user($user);
if (!empty($errors)) {
return $errors;
}

$hashed_password = password_hash($user['password'], PASSWORD_BCRYPT);


$sql = "INSERT INTO personne ";
$sql .= "(civilite,nom,prenom,adr_mail,indic,numtel,date_de_naissance,num_rue,rue,code_postal,ville,pays , mot_de_passe) ";
$sql .= "VALUES (";
$sql .= "'" . db_escape($db, $user['civilite']) . "',";
$sql .= "'" . db_escape($db, $user['nom']) . "',";
$sql .= "'" . db_escape($db, $user['prenom']) . "',";
$sql .= "'" . db_escape($db, $user['email']) . "',";
$sql .= "'" . db_escape($db, $user['indic']) . "',";
$sql .= "'" . db_escape($db, $user['numtel']) . "',";
$sql .= "'" . db_escape($db, $user['ddn']) . "',";
$sql .= "'" . db_escape($db, $user['num_rue']) . "',";
$sql .= "'" . db_escape($db, $user['rue']) . "',";
$sql .= "'" . db_escape($db, $user['code_postal']) . "',";
$sql .= "'" . db_escape($db, $user['ville']) . "',";
$sql .= "'" . db_escape($db, $user['pays']) . "',";
$sql .= "'" . db_escape($db, $hashed_password) . "'";
$sql .= ")";
$result = mysqli_query($db, $sql);

// For INSERT statements, $result is true/false
if($result) {
return true;
} else {
// INSERT failed
echo mysqli_error($db);
db_disconnect($db);
exit;
}
}

function update_user($user) {
global $db;

$errors = validate_user($user);
if (!empty($errors)) {
return $errors;
}

$hashed_password = password_hash($user['password'], PASSWORD_BCRYPT);

$sql = "UPDATE personne SET ";
$sql .= "nom='" . db_escape($db, $user['first_name']) . "', ";
$sql .= "prenom='" . db_escape($db, $user['last_name']) . "', ";
$sql .= "email='" . db_escape($db, $user['email']) . "', ";
$sql .= "mot_de_passe='" . db_escape($db, $hashed_password) . "',";
$sql .= "indic='" . db_escape($db, $user['indic']) . "',";
$sql .= "num_tel='" . db_escape($db, $user['numtel']) . "',";
$sql .= "date_de_naissance='" . db_escape($db, $user['ddn']) . "',";
$sql .= "num_rue='" . db_escape($db, $user['num_rue']) . "',";
$sql .= "rue='" . db_escape($db, $user['rue']) . "',";
$sql .= "code_postal'" . db_escape($db, $user['code_postal']) . "',";
$sql .= "ville='" . db_escape($db, $user['ville']) . "',";
$sql .= "pays='" . db_escape($db, $user['pays']) . "',";
$sql .= "statut='" . db_escape($db, $user['statut']) . "',";
$sql .= "WHERE id='" . db_escape($db, $user['id']) . "' ";
$sql .= "LIMIT 1";
$result = mysqli_query($db, $sql);

// For UPDATE statements, $result is true/false
if($result) {
return true;
} else {
// UPDATE failed
echo mysqli_error($db);
db_disconnect($db);
exit;
}
}

function delete_user($user) {
global $db;

$sql = "DELETE FROM personne ";
$sql .= "WHERE id_pers='" . db_escape($db, $user['id']) . "' ";
$sql .= "LIMIT 1;";
$result = mysqli_query($db, $sql);

// For DELETE statements, $result is true/false
if($result) {
return true;
} else {
// DELETE failed
echo mysqli_error($db);
db_disconnect($db);
exit;
}
}

?>